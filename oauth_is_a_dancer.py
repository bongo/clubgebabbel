#!/usr/bin/python2

import tweepy
import sys
import pickle

oauth = {}

if len(sys.argv) > 1:
    oafile = sys.argv[1] + ".oauth"
else:
    oafile = "clubgebabbel.oauth"

print("sign in with your twitter account and got to the following page and 'create' your app")
print("https://apps.twitter.com/app/new")
raw_input("press <enter> once you finshed")
print("Go to the 'Keys and Access Tokens' tab and c'n'p the keys when prompted")

oauth['conskey'] = raw_input("Consumer Key (Api Key): ")
oauth['conssec'] = raw_input("Consumer Secret (Api Secret): ")

auth = tweepy.OAuthHandler(oauth['conskey'], oauth['conssec'])

try:
    url = auth.get_authorization_url()
except tweepy.TweepError:
    print("Error! Faild to get request token")

print("Go the the following URL and c'n'p the given Pin when prompted")
print(url)

pin = raw_input("Pin: ")

try:
    auth.get_access_token(pin)
except tweepy.TweepError:
    print("Error! Failed to get access token!")



oauth['acctoken'] = auth.access_token
oauth['accsecret'] = auth.access_token_secret

with open(oafile, "wb") as handle:
    pickle.dump(oauth, handle)

print("You passed the dance successfully!")

